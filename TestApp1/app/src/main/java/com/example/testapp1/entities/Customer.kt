package com.example.testapp1.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "customer_info")
data class Customer(
    @PrimaryKey(autoGenerate = true) val id:Int=0,
    @ColumnInfo(name="customer_name") val name: String,
    @ColumnInfo(name="customer_email") val email:String,
    @ColumnInfo(name="customer_mobile_no") val mobileNo:String
)
