package com.example.testapp1

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.TextView
import com.example.testapp1.models.Person
import com.example.testapp1.models.User
import com.example.testapp1.providers.LocalDataProvider
import com.example.testapp1.utils.showToast
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() , View.OnClickListener {
    private lateinit var localDataProvider: LocalDataProvider

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        /*btnSignIn.setOnClickListener {
            validateUser();
        }*/

        /*btnSignIn.setOnClickListener(View.OnClickListener {
            validateUser();
        })*/

        localDataProvider = LocalDataProvider(this)

        val userObjStr = localDataProvider.getData("user_obj")
        if(userObjStr.isNotEmpty()){
            val gson = Gson();
            val user = gson.fromJson(userObjStr, User::class.java)
            goToDashboard(user)
        }

        /*val email = localDataProvider.getData("email")
        val password = localDataProvider.getData("password")

        if(email.isNotEmpty() && password.isNotEmpty()){
            edEmail.setText(email)
            edPassword.setText(password)

            val user = User(1,"Muhammad Ali", email,"045435855",password)
            goToDashboard(user)
        }*/


        btnSignIn.setOnClickListener(this)
    }

    /*fun onSignInCLicked(view: View) {
        validateUser();
    }*/

    private fun validateUser() {
        val emailRegex = Regex("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})\$")

        val email = edEmail.text.toString().trim()
        val password = edPassword.text.toString().trim()

        if(email.isNullOrEmpty()){
            showToast("Please enter your email")
            return
        }

        if(!emailRegex.matches(email)){
            showToast("Please valid email")
            return
        }

        if(password.isNullOrEmpty()){
            showToast("Please enter your password")
            return
        }

        /*val intent = Intent(this,HomeActivity::class.java)
        startActivity(intent)
        finish()*/

        /*Intent(this,DashboardActivity::class.java).apply {
            putExtra("email",email)
            putExtra("password",password)
            startActivity(this)
            finish()
        }*/

        //val person = Person(1,"Muhammad Ali", email,"045435855",password)
        val user = User(1,"Muhammad Ali", email,"045435855",password)

        //localDataProvider.saveData("email", email)
        //localDataProvider.saveData("password", password)

        val gson = Gson();
        val userStr = gson.toJson(user)
        localDataProvider.saveData("user_obj", userStr)

        goToDashboard(user)

    }

    private fun goToDashboard(user:User ){
        Intent(this,DashboardActivity::class.java).apply {
            putExtra("userObj",user)
            startActivity(this)
            finish()
        }
    }
    override fun onClick(p0: View?) {
        /*if(p0?.id == R.id.btnSignIn)
            validateUser();*/

        when(p0?.id){
            R.id.btnSignIn -> validateUser()
            else -> showToast("click event not matched")
        }
    }
}