package com.example.testapp1.providers

import android.content.Context

class LocalDataProvider(context: Context) {
    private val sharedPref = context.getSharedPreferences("pref_test_app", Context.MODE_PRIVATE)

    fun getData(key:String):String{
        return sharedPref.getString(key,"")?:""
    }

    fun saveData(key:String, value:String){
        sharedPref.edit().apply {
            putString(key, value)
            apply()
        }
    }
}