package com.example.testapp1.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class User(
    val id:Int,
    val name:String,
    val email:String,
    val mobileNo:String,
    val password:String
):Parcelable
