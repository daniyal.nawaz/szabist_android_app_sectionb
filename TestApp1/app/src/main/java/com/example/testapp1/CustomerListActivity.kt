package com.example.testapp1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.testapp1.adapters.CustomerAdapter
import com.example.testapp1.database.AppDatabase
import com.example.testapp1.entities.Customer
import com.example.testapp1.utils.showToast
import kotlinx.android.synthetic.main.activity_customer_list.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext

class CustomerListActivity : AppCompatActivity(), CoroutineScope {
    private var job: Job = Job()

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main + job

    private var adapter:CustomerAdapter? = null
    private val context = this
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_customer_list)

        rvCustomerList.layoutManager = LinearLayoutManager(this)

        getAllCustomer(1)
    }

    fun onAddCustomerClicked(view: View) {
        val name = edName.text.toString().trim()
        val email = edEmail.text.toString().trim()
        val mobileNo = edMobileNo.text.toString().trim()

        if(name.isNotEmpty() && email.isNotEmpty() && mobileNo.isNotEmpty()){
            val customer = Customer(name=name, email = email, mobileNo = mobileNo)
            addCustomer(customer)
        }
    }

    fun addCustomer(customer: Customer){
        launch {
            val id = AppDatabase.getDatabaseInstance(context).customerDao().insertOrUpdate(customer)
            showToast("Customer Added ID = ${id}")
            getAllCustomer(2)
        }
    }

    fun getAllCustomer(operation:Int){
        launch {
            val customerList = AppDatabase.getDatabaseInstance(context).customerDao().getAllCustomer()
            if(operation==1)
                renderList(customerList)
            else
                refreshList(customerList)
        }
    }

    fun renderList(customerList:List<Customer>){
        adapter = CustomerAdapter(customerList)
        rvCustomerList.adapter = adapter
    }

    fun refreshList(customerList:List<Customer>){
        adapter?.setItems(customerList)
        adapter?.notifyDataSetChanged()
        rvCustomerList.scrollToPosition(customerList.size-1)
    }

    override fun onDestroy() {
        super.onDestroy()
        job.cancel()
    }
}