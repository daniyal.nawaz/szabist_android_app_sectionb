package com.example.testapp1.dao

import androidx.constraintlayout.widget.ConstraintSet
import androidx.room.*
import com.example.testapp1.entities.Customer
import com.example.testapp1.models.Contact

@Dao
interface CustomerDAO {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertOrUpdate(customer: Customer):Long

    @Query("SELECT * FROM customer_info")
    suspend fun getAllCustomer():List<Customer>

    @Query("SELECT * FROM customer_info WHERE id=:id")
    suspend fun getCustomerByID(id:Int):Customer

    @Delete
    suspend fun delete(customer: Customer)
}