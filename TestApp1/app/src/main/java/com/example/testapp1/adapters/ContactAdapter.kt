package com.example.testapp1.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.testapp1.R
import com.example.testapp1.models.Contact
import com.example.testapp1.viewholders.ContactViewHolder

class ContactAdapter(private val contactList:ArrayList<Contact>) : RecyclerView.Adapter<ContactViewHolder>(){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContactViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_contact,parent,false)
        return ContactViewHolder(view)
    }

    override fun onBindViewHolder(holder: ContactViewHolder, position: Int) {
        val contact = contactList[position]
        holder.txName.text = contact.name
        holder.txEmail.text = contact.email
        holder.txMobileNo.text = contact.mobileNo
    }

    override fun getItemCount(): Int {
        return contactList.size
    }
}