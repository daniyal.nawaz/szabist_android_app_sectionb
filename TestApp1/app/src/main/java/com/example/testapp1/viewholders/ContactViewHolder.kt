package com.example.testapp1.viewholders

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.testapp1.R

class ContactViewHolder(itemView:View):RecyclerView.ViewHolder(itemView) {
    val image: ImageView = itemView.findViewById(R.id.imgUser)
    val txName: TextView = itemView.findViewById(R.id.txName)
    val txEmail: TextView = itemView.findViewById(R.id.txEmail)
    val txMobileNo: TextView = itemView.findViewById(R.id.txMobileNo)
}