package com.example.testapp1.models

data class Contact(
    val id:String,
    val name:String,
    val email:String,
    val mobileNo:String,
    val image:Int
)
