package com.example.testapp1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import com.example.testapp1.utils.showToast

class SplashActivity : AppCompatActivity() {
    private val handler = Handler();
    private lateinit var runnable:Runnable
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

         runnable = Runnable {
            val intent = Intent(this,CustomerListActivity::class.java)
            startActivity(intent)
            finish()
        }

        handler.postDelayed(runnable,3000)
    }

    override fun onDestroy() {
        handler.removeCallbacks(runnable)
        super.onDestroy()

        //showToast("onDestroy()")
    }
}