package com.example.testapp1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.testapp1.adapters.ContactAdapter
import com.example.testapp1.database.AppDatabase
import com.example.testapp1.entities.Customer
import com.example.testapp1.models.Contact
import com.example.testapp1.models.Person
import com.example.testapp1.models.User
import kotlinx.android.synthetic.main.activity_dashboard.*

class DashboardActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard)

        /*val email = intent?.extras?.getString("email")
        val password = intent?.extras?.getString("password")

        txDetails.text = "Email : $email\nPassword : $password"*/


        /*val person = intent?.extras?.getSerializable("personObj") as Person
        txDetails.text = "Email : ${person.email}\nPassword : ${person.password}"*/

        /*val user = intent?.extras?.getParcelable<User>("userObj")
        txDetails.text = "Email : ${user!!.email}\nPassword : ${user!!.password}"*/

        val user = intent?.extras?.getParcelable<User>("userObj")
        txDetails.text = "Hi ${user!!.email}"

        //rvContactList.layoutManager = LinearLayoutManager(this)
        //rvContactList.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        //rvContactList.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, true)
        rvContactList.layoutManager = GridLayoutManager(this, 2)

        val dataSource = getDataSource()
        val adapter = ContactAdapter(dataSource)
        rvContactList.adapter = adapter
    }

    private fun getDataSource():ArrayList<Contact>{
        val data = arrayListOf<Contact>()

        data.add(Contact("1","Yasir","yasir@abc.com","+92-344-2000001",0))
        data.add(Contact("2","Kashif","kashif@abc.com","+92-344-2000002",0))
        data.add(Contact("3","Hannan","hannan@abc.com","+92-344-2000003",0))
        data.add(Contact("4","Razi","razi@abc.com","+92-344-2000004",0))
        data.add(Contact("5","Hadi","hadi@abc.com","+92-344-2000005",0))
        AppDatabase.getDatabaseInstance(this)
        return data
    }
}