package com.example.testapp1

import android.app.DatePickerDialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import kotlinx.android.synthetic.main.activity_sign_up.*
import java.text.SimpleDateFormat
import java.util.*

class SignUpActivity : AppCompatActivity(), AdapterView.OnItemSelectedListener {
    private val cal = Calendar.getInstance()
    private val genderArr = arrayOf("Select Gender","Male","Female","Other")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)

        //Spinner - Combo Box
        //val genderAdapter = ArrayAdapter.createFromResource(this, R.array.gender_array, android.R.layout.simple_spinner_item)

        val genderAdapter = ArrayAdapter(this,android.R.layout.simple_spinner_item, genderArr)
        genderAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spGender.adapter = genderAdapter
        spGender.onItemSelectedListener = this

        updateDateOnUI()

        val dateListener = DatePickerDialog.OnDateSetListener { datePicker, i, i2, i3 ->
            cal.set(Calendar.YEAR,i)
            cal.set(Calendar.MONTH,i2)
            cal.set(Calendar.DAY_OF_MONTH,i3)
            updateDateOnUI()
        }

        icDOBPicker.setOnClickListener {
            DatePickerDialog(this@SignUpActivity,
                dateListener, cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH)
            ).show()
        }
    }

    private fun updateDateOnUI(){
        val dateFormat = "dd-MM-YYYY"
        val df = SimpleDateFormat(dateFormat, Locale.US)
        txDOB.text = df.format(cal.time).toString()
    }

    override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
        Toast.makeText(this, p0?.getItemAtPosition(p2).toString(),Toast.LENGTH_SHORT).show()
    }

    override fun onNothingSelected(p0: AdapterView<*>?) {

    }

    fun onSignUpClicked(view: View) {
        val message = "10 Above = ${rd10Above.isChecked}\n" +
                "18 Above = ${rd18Above.isChecked}" +
                "\n31 Above = ${rd31Above.isChecked}" +
                "\n\nTerms & Condition = ${chTermsConditions.isChecked}"

        AlertDialog.Builder(this).apply {
            setTitle("Title")
            setMessage(message)
            .setPositiveButton("Ok"){ dialog, i->
                dialog.dismiss()
            }
            .setNegativeButton("Cancel"){ dialog, i->
                dialog.dismiss()
            }
        }.create().show()
    }
}