package com.example.testapp1.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.testapp1.dao.CustomerDAO
import com.example.testapp1.entities.Customer

@Database(entities = arrayOf(Customer::class), version = 1, exportSchema = false)
abstract class AppDatabase:RoomDatabase() {
    abstract fun customerDao():CustomerDAO

    companion object {
        private var INSTANCE:AppDatabase? = null

        fun getDatabaseInstance(context: Context):AppDatabase{
            return INSTANCE?: synchronized(this){
                val instance = Room.databaseBuilder(context, AppDatabase::class.java,"db_test_app").build()
                INSTANCE = instance
                instance
            }
        }
    }
}