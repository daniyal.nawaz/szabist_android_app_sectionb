package com.example.testapp1.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.testapp1.R
import com.example.testapp1.entities.Customer
import com.example.testapp1.models.Contact
import com.example.testapp1.viewholders.ContactViewHolder
import com.example.testapp1.viewholders.CustomerViewHolder

class CustomerAdapter(private var customerList:List<Customer>) : RecyclerView.Adapter<CustomerViewHolder>(){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomerViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_customer,parent,false)
        return CustomerViewHolder(view)
    }

    override fun onBindViewHolder(holder: CustomerViewHolder, position: Int) {
        val customer = customerList[position]
        holder.txName.text = customer.name
        holder.txEmail.text = customer.email
        holder.txMobileNo.text = customer.mobileNo
    }

    override fun getItemCount(): Int {
        return customerList.size
    }

    fun setItems(newCustomerList:List<Customer>){
        this.customerList = newCustomerList
    }
}