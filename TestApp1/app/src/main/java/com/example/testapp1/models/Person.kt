package com.example.testapp1.models

import java.io.Serializable

data class Person(
    val id:Int,
    val name:String,
    val email:String,
    val mobileNo:String,
    val password:String
):Serializable
